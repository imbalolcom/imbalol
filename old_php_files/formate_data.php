<?php
//главная таблица
function formateMainTable($day, $day1, $day7, $r){
    $mainTable = array();
    array_push($mainTable, 
        array(
            'name'   => 'Рейтинг эффективности',
            'value'  => $r['er'],
            'value1' => null,
            'value7' => null
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'WN8 (кпд)',
            'value'  => $r['wn8'],
            'value1' => null,
            'value7' => null
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'WN7',
            'value'  => $r['wn7'],
            'value1' => null,
            'value7' => null
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'Количество боев',
            'value'  => $day['all']['battles'],
            'value1' => $day1['battles_count']['value'],
            'value7' => $day7['battles_count']['value']
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'Процент побед',
            'value'  => round($day['all']['wins']/$day['all']['battles']*100,2),
            'value1' => $day1['wins_ratio']['value'],
            'value7' => $day7['wins_ratio']['value']
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'Средний урон',
            'value'  => round($day['all']['damage_dealt']/$day['all']['battles'],2),
            'value1' => $day1['damage_avg']['value'],
            'value7' => $day7['damage_avg']['value']
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'Средний опыт',
            'value'  => $day['all']['battle_avg_xp'],
            'value1' => $day1['xp_avg']['value'],
            'value7' => $day7['xp_avg']['value']
        )
    );    

    array_push($mainTable, 
        array(
            'name'   => 'Максимальный опыт за бой',
            'value'  => $day['all']['max_xp'],
            'value1' => $day1['xp_max']['value'],
            'value7' => $day7['xp_max']['value']
        )
    );    
        
    array_push($mainTable, 
        array(
            'name'   => 'Уничтожено врагов',
            'value'  => round($day['all']['frags']/$day['all']['battles'],2),
            'value1' => $day1['frags_avg']['value'],
            'value7' => $day7['frags_avg']['value']
        )
    );  

    array_push($mainTable, 
        array(
            'name'   => 'Выжил в битвах',
            'value'  => round($day['all']['survived_battles']/$day['all']['battles'],2),
            'value1' => $day1['survived_ratio']['value'],
            'value7' => $day7['survived_ratio']['value']
        )
    );      

    return $mainTable;
}

//мелкие графики
function formateLineCharts($day, $r, $arr7days, $db_data){
    $arrDmg = array();
    $arrXp = array();
    $arrWn8 = array();
    $arrWn7 = array();
    $arrEr = array();

    if ($db_data == null){
        foreach ($arr7days as $key => $value) {
              array_push($arrDmg, $value['damage_avg']['value']);
              array_push($arrXp, $value['xp_avg']['value']);
        };

        return array(
            'mainChart' => array('name'=> 'Рейтинг эффективности', 'value' => $r['er'], 'values'=> array(array(0,0),array($day['all']['battles'],$r['er']))),
            'win8Chart' => array('name'=> 'WN8', 'value' => $r['wn8'], 'values' => array(0, $r['wn8'])),
            'win7Chart' => array('name'=> 'WN7', 'value' => $r['wn7'], 'values' => array(0, $r['wn7'])),
            'damageChart' => array('name'=> 'Средний урон', 'value' => round($day['all']['damage_dealt']/$day['all']['battles'],2), 'values' => $arrDmg),
            'xpChart' => array('name'=> 'Средний опыт', 'value' => $day['all']['battle_avg_xp'], 'values' => $arrXp),
        );
    }
    else
    {   
        $min = 100000;
        $max = 0;
        foreach ($db_data as $key => $value) {
              array_push($arrDmg, $value['damage_avg']['value']);
              array_push($arrXp, $value['xp_avg']['value']);
              array_push($arrWn8, $value['wn8']['value']);
              array_push($arrWn7, $value['wn7']['value']);
              array_push($arrEr, array($value['battles_count']['value'], $value['effencive_ratio']['value']));

              if ($value['effencive_ratio']['value'] > $max) { $max = $value['effencive_ratio']['value']; }
              if ($value['effencive_ratio']['value'] < $min) { $min = $value['effencive_ratio']['value']; }
        };
        if ($min == $max) { $max = $min + 5; $min = $min - 5;};
        if ($max - $min < 5) { $max = $min +5; };
        if ($min > 5) { $min = $min -5;};
        return array(
            'mainChart' => array('name'=> 'Рейтинг эффективности', 'value' => $r['er'], 'values'=> $arrEr, 'min' => $min, 'max' => $max),
            'win8Chart' => array('name'=> 'WN8', 'value' => $r['wn8'], 'values' => $arrWn8),
            'win7Chart' => array('name'=> 'WN7', 'value' => $r['wn7'], 'values' => $arrWn7),
            'damageChart' => array('name'=> 'Средний урон', 'value' => round($day['all']['damage_dealt']/$day['all']['battles'],2), 'values' => $arrDmg),
            'xpChart' => array('name'=> 'Средний опыт', 'value' => $day['all']['battle_avg_xp'], 'values' => $arrXp),
        );
    }
}
//круглые графики
function formatePieCharts($tanks, $tankopedia){
    $models = array();
    $countries = array();
    $countriesAll = array();
    $modelsAll = array();
    //array_push($countriesAll, $tankopedia);
    //array_push($countriesAll, $tankopedia['51713']['nation_i18n']);
    //return $countriesAll;
    foreach ($tanks as $key => $value) {
        
        if ($countriesAll[$tankopedia[$value['tank_id']]['nation_i18n']] == null)
        {
            $countriesAll[$tankopedia[$value['tank_id']]['nation_i18n']] = array('name' => $tankopedia[$value['tank_id']]['nation_i18n'], 'count' => $value['statistics']['battles']);  
        }
        else
        {
            $countriesAll[$tankopedia[$value['tank_id']]['nation_i18n']]['count'] +=  $value['statistics']['battles'];
        }

        if ($modelsAll[$tankopedia[$value['tank_id']]['type_i18n']] == null)
        {
            $modelsAll[$tankopedia[$value['tank_id']]['type_i18n']] = array('name' => $tankopedia[$value['tank_id']]['type_i18n'], 'count' => $value['statistics']['battles']);  
        }
        else
        {
            $modelsAll[$tankopedia[$value['tank_id']]['type_i18n']]['count'] +=  $value['statistics']['battles'];
        }        
    }

    $i = 0.0;
    foreach ($countriesAll as $key => $value) {
        if ($value['count'] > 5){
            $i += 0.1;
            array_push($countries, array('data' => $value['count'], 'color' => 'rgba(255,255,255,'.$i.')', 'label' => $value['name']));
        }
    }

    foreach ($modelsAll as $key => $value) {
        if ($value['count'] > 5){
            $i += 0.1;
            array_push($models, array('data' => $value['count'], 'color' => 'rgba(255,255,255,'.$i.')', 'label' => $value['name']));
        }
    } 

    return array('countries' => $countries, 'models' => $models);
}
     /*   var models = [];
        var countries = [];

        var modelsAll = TechLibary.data;

        var countriesAll = {};
        var typesAll = {};

        angular.forEach(tanks, function(data){
            if (data.tank_id !== null)
            {
                if (typeof(countriesAll[modelsAll[data.tank_id].nation_i18n]) == 'undefined')
                {
                    countriesAll[modelsAll[data.tank_id].nation_i18n] = {name: modelsAll[data.tank_id].nation_i18n, count : parseInt(data.statistics.battles)};
                }
                else
                {
                    countriesAll[modelsAll[data.tank_id].nation_i18n].count += parseInt(data.statistics.battles);
                }

                if (typeof(typesAll[modelsAll[data.tank_id].type_i18n]) == 'undefined')
                {
                    typesAll[modelsAll[data.tank_id].type_i18n] = {name: modelsAll[data.tank_id].type_i18n, count : parseInt(data.statistics.battles)};
                }
                else
                {
                    typesAll[modelsAll[data.tank_id].type_i18n].count += parseInt(data.statistics.battles);
                }
            }
        });  
        var i = 0.0;
        angular.forEach(countriesAll, function(c){
            if (c.count > 5) {
                i += 0.1;
                countries.push({data: c.count, color: 'rgba(255,255,255,'+ i +')', label: c.name});
            }
        });
        viewPieChart('#pie-chart2',countries);

        angular.forEach(typesAll, function(c){
            if (c.count > 5) {
                i += 0.1;
                models.push({data: c.count, color: 'rgba(255,255,255,'+ i +')', label: c.name});
            }
        });

        
        viewPieChart('#pie-chart1',models);*/

?>