<?php

function checkUser($userName){
    $user = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/account/list/?application_id='.$GLOBALS["appID"].'&type=exact&search='.$userName), true);
   // return json_encode($user);//['data']['account_id'];
    if ($user['status'] == 'ok' && $user['meta']['count'] > 0)
    {
        return $user['data'][0]['account_id'];
    }
    else
    {
        return -1;
    }
}

// статиcтика за 7 дней
function getOnDates($account_id){
    $dates = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/ratings/dates/?application_id='.$GLOBALS["appID"].'&type=all'), true);
    $allOnDates = array();
    foreach ($dates['data']['all']['dates'] as $key => $value) {
        //$stat = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/ratings/accounts/?application_id='.$GLOBALS["appID"].'&type=all&account_id='.$account_id.'&date=' . $value), true);
        $stat = getStatByType($account_id, 'all', $value);
        array_push($allOnDates, $stat);
    }
    return $allOnDates;
    //echo json_encode($allOnDates);
}

function getStatByType($account_id, $type, $date){
    //$stat = array();
    if ($date != null)
    {
        $stat = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/ratings/accounts/?application_id='.
                $GLOBALS["appID"].'&type='.$type.'&account_id='.$account_id.'&date=' . $date), true);
    }
    else
    {
        $stat = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/ratings/accounts/?application_id='.
                $GLOBALS["appID"].'&type='.$type.'&account_id='.$account_id), true);        
    }
    return $stat['data'][$account_id];
}

function getAccountTechStats($account_id){
    // техника аккаунта
    $tanks = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/tanks/stats/?application_id='.$GLOBALS["appID"].'&account_id='.$account_id), true);
    return $tanks['data'][$account_id];
}
  
function getAccountTech($account_id){
    // техника аккаунта
    $tanks = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/account/tanks/?application_id='.$GLOBALS["appID"].'&account_id='.$account_id), true);
    return $tanks;
}
  
function getInfo($account_id){
    // общая информация аккаунта
    $statistics = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/account/info/?application_id='.$GLOBALS["appID"].'&account_id='.$account_id),true);
    return $statistics;
}

function getAllTech(){
    // справочник танков
    $tankopedia = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/encyclopedia/tanks/?application_id='.$GLOBALS["appID"]),true);
    return $tankopedia;
}
function getTechExp(){
    // експ танков на текущую дату
    $exp1 = json_decode(file_get_contents('http://www.wnefficiency.net/exp/expected_tank_values_latest.json'),true);
    //$exp = ;
    return $exp1['data'];
}

function getClanInfo($clan_id){
    // общая информация клана
    $clan = json_decode(file_get_contents('http://api.worldoftanks.ru/wot/clan/info/?application_id='.$GLOBALS["appID"].'&clan_id='.$clan_id),true);
    return $clan['data'][$clan_id];
}


// не используеся! оставил для примера использования curl
function curl_get_contents($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

?>