<?php
date_default_timezone_set('UTC');
$db_host = '';
$db_user = '';
$db_password = '';

//echo date('Y-m-d');
//sqlData(3,'aassd');
function sqlData($account_id, $user_name, $day, $r){
    //echo '<meta charset="UTF-8">';
    // Соединяемся, выбираем базу данных
    $link = mysql_connect($GLOBALS["db_host"], $GLOBALS["db_user"], $GLOBALS["db_password"])
        or die('Не удалось соединиться: ' . mysql_error());
    mysql_select_db('gb_tanks_db') or die('Не удалось выбрать базу данных');

    $query = 'SELECT count(1) FROM accounts_tanks where account_id='.$account_id;
    //$result = mysql_query($query) or die('Запрос не удался: ' . mysql_error());

    $cnt = execScalarSql($query);
    // если в базе такого пользователя ещё нет нужно создать
    if ($cnt == 0){
        createAccount($account_id, $user_name);
    };
    // проверяем наличие статистики на сегодня
    $query = 'SELECT count(1) FROM account_stats where account_id='.$account_id.' and on_date="'.date('Y-m-d').'"';
    $cnt = execScalarSql($query);
    // если в базе статистики нет нужно создать
    if ($cnt == 0){
        $er = 1000.123;
        createStat($account_id, date('Y-m-d'), $day, $r);
    };

    // проверяем на сколько дат есть статистика
    $query = 'SELECT count(1) FROM account_stats where account_id='.$account_id;
    $cnt = execScalarSql($query);
    if ($cnt > 1){
        $result = createStatArray($account_id);
    }
    else
    {
        $result = null;
    }
    // Закрываем соединение
    mysql_close($link);
    return $result;
}

function createAccount($account_id, $user_name){
    $query = 'INSERT INTO accounts_tanks (account_id, user_name, date_in) VALUES ('.$account_id.', "'.$user_name.'", CURRENT_TIMESTAMP)';
    execProcSql($query);
}

function createStat($account_id, $date, $day, $r){
    $query = 'INSERT INTO `gb_tanks_db`.`account_stats` (`stat_id`, `account_id`, `on_date`, `effencive_ratio`, `wn8`, `wn7`, `battles_count`, `wins_ratio`, `damage_avg`, `xp_avg`, 
                          `xp_max`, `frage_avg`, `survived_ratio`) 
    VALUES (NULL, '.$account_id.', "'.$date.'", '.$r['er'].', '.$r['wn8'].', '.$r['wn7'].
            ', '.$day['all']['battles'].
            ', '.round($day['all']['wins']/$day['all']['battles']*100,2).
            ', '.round($day['all']['damage_dealt']/$day['all']['battles'],2).
            ', '.$day['all']['battle_avg_xp'].
            ', '.$day['all']['max_xp'].
            ', '.round($day['all']['frags']/$day['all']['battles'],2).
            ', '.round($day['all']['survived_battles']/$day['all']['battles'],2).');';
    execProcSql($query);
}

function execScalarSql($query){
    // Выполняем SQL-запрос
    $result = mysql_query($query);
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        foreach ($line as $col_value) {
            $val = $col_value;
        }
    }
    // Освобождаем память от результата
    mysql_free_result($result);
    return $val;
}

function createStatArray($account_id){
    $query = 'SELECT * FROM account_stats where account_id='.$account_id.' ORDER BY on_date';
    // Выполняем SQL-запрос
    $result = mysql_query($query);
    $resArray = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $lineArray = array();
        $lineArray['effencive_ratio'] = array('value' => $line['effencive_ratio']);
        $lineArray['wn8'] = array('value' => $line['wn8']);
        $lineArray['wn7'] = array('value' => $line['wn7']);
        $lineArray['battles_count'] = array('value' => $line['battles_count']);
        $lineArray['damage_avg'] = array('value' => $line['damage_avg']);
        $lineArray['xp_avg'] = array('value' => $line['xp_avg']);
        array_push($resArray, $lineArray);
    }
    // Освобождаем память от результата
    mysql_free_result($result);
    return $resArray;
}


function execProcSql($query){
    // Выполняем SQL-запрос
    mysql_query($query);
}

?>