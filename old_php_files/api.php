<?php 
include('load_data.php');
include('formate_data.php');
include('sql.php');

$appID = '4936fa560dbbfb1b74c74755fbd6b36c';

if(function_exists($_GET['method'])){
    if(!empty($_GET['account_id'])){
        header('Content-Type: application/json');
        $_GET['method']($_GET['account_id']);
    }
    
}

function userInfo($user_name){
    $account_id = checkUser($user_name);
    // не нашлся пользователь - лесом.
    if ($account_id == -1)
    {   
        echo json_encode(array('account_id' => -1));
        return;
    }    
    $statistics = getInfo($account_id);
    $clan = getClanInfo($statistics['data'][$account_id]['clan_id']);

    echo json_encode(array( 'account_id' => $account_id,
                'clan_id' => $statistics['data'][$account_id]['clan_id'],
                'name' => $statistics['data'][$account_id]['nickname'],
                'clan_name' => $clan['abbreviation'],
                'emblem' => $clan['emblems']['large']
        ));
}

// methods
function mainPage($user_name){
    $account_id = checkUser($user_name);
    // не нашлся пользователь - лесом.
    if ($account_id == -1)
    {
        echo json_encode(array('account_id' => -1));
        return;
    }
    // техника аккаунта
    $tanks = getAccountTech($account_id);
    // общая информация аккаунта
    $statistics = getInfo($account_id);
    // справочник танков
    $tankopedia = getAllTech();
    // експ танков на текущую дату
    $exp = getTechExp();

    $day1 = getStatByType($account_id,'1', null);
    $day7 = getStatByType($account_id,'7', null);
    $day = $statistics['data'][$account_id]['statistics'];
      //getStatByType($account_id,'all', null);  

    $wn8 = wn8($statistics['data'][$account_id],$tanks['data'][$account_id],$exp);
    $wn7 = wn7($statistics, $tanks, $tankopedia, $account_id);
    $er = er($statistics, $tanks, $tankopedia, $account_id);
    $r = array('wn8' => $wn8,
               'wn7' => $wn7,
               'er' => $er
              );  
    $db_data = sqlData($account_id, $user_name, $day, $r);
    // если 1е посещение грузим данные
    if ($db_data == null)
    {
        $arr7days = getOnDates($account_id);
        $charts = formateLineCharts($day, $r, array_reverse($arr7days), null);
    }
    // если второе и более забиваем
    else
    {
        $charts = formateLineCharts($day, $r, null, $db_data);
    }
    

    //начинаем заполнять данные
    $mainTable = formateMainTable($day, $day1, $day7, $r);
    
    $pieCharts = formatePieCharts($tanks['data'][$account_id], $tankopedia['data']);

    $result = array(
        'mainTable' => $mainTable,
        'charts' => $charts,
        'pieCharts' => $pieCharts
    );
    echo json_encode($result);
}

//техника
function techPage($user_name){
    $account_id = checkUser($user_name);
    // не нашлся пользователь - лесом.
    if ($account_id == -1)
    {
        echo json_encode(array('account_id' => -1));
        return;
    }
    //short_name_i18n
    // техника аккаунта
    //$tanks = getAccountTech($account_id);
    // статистика по технике
    $tanksStats = getAccountTechStats($account_id);  
    // експ танков на текущую дату
    $exp = getTechExp();
    $tanks = $tanks['data'][$account_id];
    // справочник танков
    $tankopedia = getAllTech();
    $tankopedia = $tankopedia['data'];
    $result = array();
    foreach ($tanksStats as $key => $value) {
        $wn8 = wn8Tank($value, $exp);
        $expDmg  = $expTankData["expDamage"];
        $expSpot = $expTankData["expSpot"];
        $expFrag = $expTankData["expFrag"];
        $expDef  = $expTankData["expDef"];
        $expWin  = $expTankData["expWinRate"];  
        if ($value['all']['battles'] == 0){
          $winRate = 0;
        }
        else {
          $winRate = round($value['all']['wins'] / $value['all']['battles'] * 100,2);
        }
        array_push($result, array('name' => $tankopedia[$value['tank_id']]['name_i18n'],
                                  'type' => $tankopedia[$value['tank_id']]['type_i18n'],
                                  'nation' => $tankopedia[$value['tank_id']]['nation_i18n'],
                                  'lvl' => $tankopedia[$value['tank_id']]['level'],
                                  'battles' => $value['all']['battles'],
                                  'win_rate' => $winRate,
                                  'wn8' => $wn8['wn8'] ?  $wn8['wn8'] : 0,
                                  'dmg' => round($wn8['rDmgc'],2),
                                  'wr' => round($wn8['rWinc'],2),
                                  'spot' => round($wn8['rSpotc'],2),
                                  'frag' => round($wn8['rFragc'],2)
             
            ));
    }

    echo json_encode($result);
}

function getWN8($account_id){
    // техника аккаунта
    $tanks = getAccountTech($account_id);
    // общая информация аккаунта
    $statistics = getInfo($account_id);
    // справочник танков
    $tankopedia = getAllTech();
    // експ танков на текущую дату
    $exp = getTechExp();

    $wn8 = wn8($statistics['data'][$account_id],array($tanks['data'][$account_id][0]),$exp);
    $wn7 = wn7($statistics, $tanks, $tankopedia, $account_id);
    $er = er($statistics, $tanks, $tankopedia, $account_id);
    $r = array('wn8' => $wn8,
               'wn7' => $wn7,
               'er' => $er
              );
    echo json_encode($r);
}


function wn8Tank($data,$exp) {
        $expDmg=0;
        $expSpot=0;
        $expFrag=0;
        $expDef=0;
        $expWin=0;
       
            $expTankData = getExp($data["tank_id"],$exp);
            $vehBattles  = $data["all"]["battles"];
            $expDmg  += $vehBattles * $expTankData["expDamage"];
            $expSpot += $vehBattles * $expTankData["expSpot"];
            $expFrag += $vehBattles * $expTankData["expFrag"];
            $expDef  += $vehBattles * $expTankData["expDef"];
            $expWin  += $vehBattles * $expTankData["expWinRate"];

        if (!$expDmg || !$expSpot || !$expFrag || !$expDef) return 0;
        $battles = $data["all"]["battles"];
        $rDmg  = $data["all"]["damage_dealt"] / $expDmg;
        $rSpot = $data["all"]["spotted"] / $expSpot;
        $rFrag = $data["all"]["frags"] / $expFrag;
        $rDef  = $data["all"]["dropped_capture_points"] / $expDef;
        $rWin  = ($data["all"]["wins"] / (($expWin * $battles)/$battles)) * 100;
       
        $rWinc  = max(0, ($rWin - 0.71) / (1-0.71));
        $rDmgc  = max(0, ($rDmg - 0.22) / (1-0.22));
        $rFragc = max(0, min($rDmgc+0.2, ($rFrag - 0.12) / (1-0.12)));
        $rSpotc = max(0, min($rDmgc+0.1, ($rSpot - 0.38) / (1-0.38)));
        $rDefc  = max(0, min($rDmgc+0.1, ($rDef - 0.10)  / (1-0.10)));
       
        return array('wn8' => round(980*$rDmgc + 210*$rDmgc*$rFragc + 155*$rFragc*$rSpotc + 75*$rDefc*$rFragc + 145*min(1.8,$rWinc)),
                     'rWinc' => $rWinc,
                     'rDmgc' => $rDmgc,
                     'rFragc' => $rFragc,
                     'rSpotc' => $rSpotc
               );      
    }

function wn8($data,$tankData,$exp) {
        $expDmg=0;
        $expSpot=0;
        $expFrag=0;
        $expDef=0;
        $expWin=0;
       
        foreach ($tankData as $key => $value) {
            $expTankData = getExp($value["tank_id"],$exp);
            $vehBattles  = $value["statistics"]["battles"];
            $expDmg  += $vehBattles * $expTankData["expDamage"];
            $expSpot += $vehBattles * $expTankData["expSpot"];
            $expFrag += $vehBattles * $expTankData["expFrag"];
            $expDef  += $vehBattles * $expTankData["expDef"];
            $expWin  += $vehBattles * $expTankData["expWinRate"];
        }
        if (!$expDmg || !$expSpot || !$expFrag || !$expDef) return 0;
        $battles = $data["statistics"]["all"]["battles"];
        $rDmg  = $data["statistics"]["all"]["damage_dealt"] / $expDmg;
        $rSpot = $data["statistics"]["all"]["spotted"] / $expSpot;
        $rFrag = $data["statistics"]["all"]["frags"] / $expFrag;
        $rDef  = $data["statistics"]["all"]["dropped_capture_points"] / $expDef;
        $rWin  = ($data["statistics"]["all"]["wins"] / (($expWin * $battles)/$battles)) * 100;
       
        $rWinc  = max(0, ($rWin - 0.71) / (1-0.71));
        $rDmgc  = max(0, ($rDmg - 0.22) / (1-0.22));
        $rFragc = max(0, min($rDmgc+0.2, ($rFrag - 0.12) / (1-0.12)));
        $rSpotc = max(0, min($rDmgc+0.1, ($rSpot - 0.38) / (1-0.38)));
        $rDefc  = max(0, min($rDmgc+0.1, ($rDef - 0.10)  / (1-0.10)));
       
        return round(980*$rDmgc + 210*$rDmgc*$rFragc + 155*$rFragc*$rSpotc + 75*$rDefc*$rFragc + 145*min(1.8,$rWinc));
    }
 
function getExp($tank_id,$exp) {
        foreach($exp as $exp_item) {
            if ($exp_item['IDNum'] == $tank_id) {
                return array(
                    'expFrag' => $exp_item['expFrag'],
                    'expDamage' => $exp_item['expDamage'],
                    'expSpot' => $exp_item['expSpot'],
                    'expDef' => $exp_item['expDef'],
                    'expWinRate' => $exp_item['expWinRate']
                );  
            }
        }
    }



function wn7($playerInfos, $playerTanks, $tankopedia, $playerId)
{
    $tankopedia = $tankopedia['data'];
    $playerTanks = $playerTanks['data'][$playerId];
    $playerInfos = $playerInfos['data'][$playerId];   
    $GAMESPLAYED = $playerInfos['statistics']['all']['battles'];
    if (!$GAMESPLAYED) return 0;
    $FRAGS = $playerInfos['statistics']['all']['frags'] / $GAMESPLAYED;
    $DAMAGE = $playerInfos['statistics']['all']['damage_dealt'] / $GAMESPLAYED;
    $SPOT = $playerInfos['statistics']['all']['spotted'] / $GAMESPLAYED;
    $DEF = $playerInfos['statistics']['all']['dropped_capture_points'] / $GAMESPLAYED;
    $WINRATE =     $playerInfos['statistics']['all']['wins'] / $GAMESPLAYED * 100;

    $weightedTier = 0;

    foreach($playerTanks as $playerTank)
    {
        $id = $playerTank['tank_id'];
        $battles = $playerTank['statistics']['battles'];
        $tier = $tankopedia[$id]['level'];

        $weightedTier += $tier * $battles;
    }

    $TIER = $weightedTier / $GAMESPLAYED;

    $WN7 = (1240 - 1040 / pow(min($TIER, 6), 0.164)) * $FRAGS;
    $WN7 += $DAMAGE * 530 / (184 * pow(M_E, (0.24 * $TIER)) + 130);
    $WN7 += $SPOT * 125 * min($TIER, 3) / 3;
    $WN7 += min($DEF, 2.2) * 100;
    $WN7 += ((185 / (0.17 + pow(M_E, (($WINRATE - 35) * -0.134)))) - 500) * 0.45;
    $WN7 -= ((5 - min($TIER, 5)) * 125) / (1 + pow(M_E, ($TIER - pow(($GAMESPLAYED / 220), (3 / $TIER))) * 1.5));

    return round($WN7);
}


function er($playerInfos, $playerTanks, $tankopedia, $playerId)
{
    $tankopedia = $tankopedia['data'];
    $playerTanks = $playerTanks['data'][$playerId];
    $playerInfos = $playerInfos['data'][$playerId];   
    $GAMESPLAYED = $playerInfos['statistics']['all']['battles'];
    if (!$GAMESPLAYED) return 0;
    $FRAGS = $playerInfos['statistics']['all']['frags'] / $GAMESPLAYED;
    $DAMAGE = $playerInfos['statistics']['all']['damage_dealt'] / $GAMESPLAYED;
    $SPOT = $playerInfos['statistics']['all']['spotted'] / $GAMESPLAYED;
    $DEF = $playerInfos['statistics']['all']['dropped_capture_points'] / $GAMESPLAYED;
    $CAP= $playerInfos['statistics']['all']['capture_points'] / $GAMESPLAYED;
    
    $WINRATE =     $playerInfos['statistics']['all']['wins'] / $GAMESPLAYED ;

    $weightedTier = 0;

    foreach($playerTanks as $playerTank)
    {
        $id = $playerTank['tank_id'];
        $battles = $playerTank['statistics']['battles'];
        $tier = $tankopedia[$id]['level'];

        $weightedTier += $tier * $battles;
    }

    $TIER = $weightedTier / $GAMESPLAYED;

    $ER = $DAMAGE * (10 / ($TIER + 2)) * (0.23 + 2* $TIER / 100);
    $ER += $FRAGS * 250;
    $ER += $SPOT * 150;
    $ER += log($CAP + 1, 1.732)  * 150;
    $ER += $DEF * 150;
    
    return round($ER);
}

?>