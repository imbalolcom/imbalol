class Account < ActiveRecord::Base
  has_many :account_statistics
  has_many :account_tanks
end
