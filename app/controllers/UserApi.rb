require "net/http"
require "uri"
require "json"

class UserApi

  APP_ID = Rails.application.secrets.app_id
  API_URL = "http://api.worldoftanks.ru/wot/"
  API_URI = URI.parse(API_URL)
  #прокси для разработки. если будет прямой коннект - закомментить и урать из вызова
  PROXY_ADDR = "172.16.1.69"
  PROXY_PORT = 3128
  @@http = Net::HTTP.new(API_URI.host, API_URI.port)
  @@exp = nil
  @@tankopedia = nil
  attr_accessor :user
  attr_accessor :user_name
  attr_accessor :account_id
  attr_accessor :clan_id
  attr_accessor :clan_name
  attr_accessor :clan_emblem
  attr_accessor :id
  attr_accessor :all_info


  def initialize(user)
    @user = user
    @account_id = exists?
    @@exp ||= get_exp
    @@tankopedia ||= get_tankopedia

  end

  def info
    if @account_id
      get_short_info
      #ищем инфу по пользователю в модели
      data = Account.where(account_id: @account_id ).first
      #если не нашли - создаем
      if data.nil?
        data = Account.create( account_id: @account_id, user_name: @user_name)
      end
      @id = data.id
      #ищем статистику по пользователю в модели
      first_date = DateTime.new(DateTime.now.year, DateTime.now.month, DateTime.now.day, 0,0,0,0)
      second_date = DateTime.new(DateTime.now.year, DateTime.now.month, DateTime.now.day, 23,59,59,59)
      stat_data = data.account_statistics.where(on_date: first_date..second_date).first

      #не нашли - создаем
      #если нет статистики за день, то нет и статистики по танкам
      if stat_data.nil?
        tech = get_account_tech
        #exp = get_exp
        #tankopedia = get_tankopedia
        wn8 = get_wn8( @all_info, tech, @@exp)
        wn7 = get_wn7( @all_info, tech, @@tankopedia)
        er = get_er( @all_info, tech, @@tankopedia)

        AccountStatistic.create(
            account_id: @id,
            on_date: first_date,
            effencive_ratio: er,
            wn8: wn8,
            wn7: wn7,
            wins_count: @all_info['statistics']['all']['wins'],
            damage_dealt: @all_info['statistics']['all']['damage_dealt'],
            survived_battles: @all_info['statistics']['all']['survived_battles'],
            frags: @all_info['statistics']['all']['frags'],
            xp: @all_info['statistics']['all']['xp'],
            battles_count: @all_info['statistics']['all']['battles'].to_f,
            wins_ratio: @all_info['statistics']['all']['wins'] / @all_info['statistics']['all']['battles'].to_f * 100,
            damage_avg: @all_info['statistics']['all']['damage_dealt'] / @all_info['statistics']['all']['battles'].to_f,
            xp_avg: @all_info['statistics']['all']['battle_avg_xp'],
            xp_max: @all_info['statistics']['all']['max_xp'],
            frage_avg: @all_info['statistics']['all']['frags'] / @all_info['statistics']['all']['battles'].to_f,
            survived_ratio: @all_info['statistics']['all']['survived_battles'] / @all_info['statistics']['all']['battles'].to_f * 100,
            dropped_capture_points: @all_info['statistics']['all']['dropped_capture_points'],
            spotted: @all_info['statistics']['all']['spotted'],
            capture_points: all_info['statistics']['all']['capture_points']
        )

        tech_stats = get_account_tech_stats

        tech_stats.each do |value|
          tank_stat = get_wn8_tank(value, @@exp)
          AccountTank.create(
              tank_id: value["tank_id"],
              tank_name: @@tankopedia["#{value["tank_id"]}"]['name_i18n'],
              tank_type: @@tankopedia["#{value["tank_id"]}"]['type_i18n'],
              tank_nation: @@tankopedia["#{value["tank_id"]}"]['nation_i18n'],
              tank_lvl: @@tankopedia["#{value["tank_id"]}"]['level'],
              account_id: @id,
              on_date: first_date,
              battles: value['all']['battles'],
              wins: value['all']['wins'],
              damage_dealt: value["all"]["damage_dealt"],
              spotted: value["all"]["spotted"],
              frags: value["all"]["frags"],
              dropped_capture_points: value["all"]["dropped_capture_points"],
              win_rate: value['all']['wins'] / value['all']['battles'].to_f * 100,

              wn8: tank_stat["wn8"],
              r_dmg_c: tank_stat["r_dmgc"],
              r_win_c: tank_stat["r_winc"],
              r_spot_c: tank_stat["r_spotc"],
              r_frag_c: tank_stat["r_fragc"]
          )
        end
      end
    end

    self
  end

  def get_stat_by_days(days = 1)
    data = Account.where(account_id: @account_id ).first
    if !data.nil?
      day_stat = ApplicationController.new.execute_statement_sql(
          "SELECT t.battles_count - t2.battles_count battles
                  ,t.damage_dealt - t2.damage_dealt damage_dealt
                  ,COALESCE(t.spotted - t2.spotted,0) spotted
                  ,COALESCE(t.frags - t2.frags,0) frags
                  ,COALESCE(t.dropped_capture_points - t2.dropped_capture_points,0) dropped_capture_points
                  ,COALESCE(t.capture_points - t2.capture_points,0) capture_points
                  ,COALESCE(t.wins_count - t2.wins_count,0) wins
                  ,COALESCE(t.xp - t2.xp,0) xp
                  ,COALESCE(t.survived_battles - t2.survived_battles,0) survived_battles
                  ,xp.xp_max
            FROM account_statistics t
                JOIN account_statistics t2 ON t2.account_id = t.account_id
                  AND t2.on_date = DATE_ADD(CURDATE(), INTERVAL -#{days} day)
                join (SELECT t.account_id, MAX(t.xp_max) xp_max
                      FROM account_statistics t
                      WHERE t.on_date <= CURDATE()
                            and t.on_date >= DATE_ADD(CURDATE(), INTERVAL -#{days} day)
                      GROUP by t.account_id) xp on xp.account_id = t.account_id
            WHERE t.on_date = CURDATE()
                  AND COALESCE(t.battles_count - t2.battles_count, 0) != 0
                  and t.account_id = #{data.id}"
      )

      day_tanks = ApplicationController.new.execute_statement_sql(
          "SELECT t.tank_id
                  ,t.tank_lvl
                  ,COALESCE(t.battles - COALESCE(t2.battles,0), 0) battles
            FROM account_tanks t
                LEFT JOIN account_tanks t2 ON t2.account_id = t.account_id
                  AND t.tank_id = t2.tank_id
                  AND t2.on_date = DATE_ADD(CURDATE(), INTERVAL -#{days} day)
            WHERE t.on_date = CURDATE()
                  AND COALESCE(t.battles - COALESCE(t2.battles,0), 0) != 0
                  and t.account_id = #{data.id}"
      )
      if !day_stat.first.nil?
        all_info = {'statistics' => {'all' => day_stat.first}}
        all_tanks = []
        day_tanks.each do |tank|
          all_tanks << {
              'tank_id' => tank['tank_id'],
              'statistics' => tank
          }
        end

        wn8 = get_wn8(all_info, all_tanks)
        wn7= get_wn7(all_info, all_tanks)
        er= get_er(all_info, all_tanks)

        return {
          'wn8' => wn8,
          'wn7' => wn7,
          'er' => er,
          'battles' => day_stat.first['battles'],

          'wins_ratio' => (day_stat.first['wins'] / day_stat.first['battles'].to_f * 100).round(2),
          'damage_avg' => (day_stat.first['damage_dealt'] / day_stat.first['battles'].to_f).round(2),
          'xp_avg' => (day_stat.first['xp'] / day_stat.first['battles'].to_f).round(2),
          'xp_max' => day_stat.first['xp_max'],
          'frage_avg' => (day_stat.first['frags'] / day_stat.first['battles'].to_f).round(2),
          'survived_ratio' => (day_stat.first['survived_battles'] / day_stat.first['battles'].to_f * 100).round(2)
        }
      else
        return {
            'wn8' => nil,
            'wn7' => nil,
            'er' => nil,
            'battles' => nil,
            'wins_ratio' => nil,
            'damage_avg' => nil,
            'xp_avg' => nil,
            'xp_max' => nil,
            'frage_avg' => nil,
            'survived_ratio' => nil,
        }
      end
    else
      return nil
    end
  end

  def get_wn8_tank(value, exp = @@exp )
    exp_dmg=0
    exp_spot=0
    exp_frag=0
    exp_def=0
    exp_win=0
    exp_tank_data = get_tank_exp(value["tank_id"], exp)
    veh_battles  = value["all"]["battles"].to_f
    exp_dmg  += veh_battles * exp_tank_data["expDamage"]
    exp_spot += veh_battles * exp_tank_data["expSpot"]
    exp_frag += veh_battles * exp_tank_data["expFrag"]
    exp_def  += veh_battles * exp_tank_data["expDef"]
    exp_win  += veh_battles * exp_tank_data["expWinRate"]

    if !exp_dmg || !exp_spot || !exp_frag || !exp_def
      return nil
    end

    battles = value["all"]["battles"].to_f
    r_dmg  = value["all"]["damage_dealt"] / exp_dmg
    r_spot = value["all"]["spotted"] / exp_spot
    r_frag = value["all"]["frags"] / exp_frag
    r_def  = value["all"]["dropped_capture_points"] / exp_def
    r_win  = (value["all"]["wins"] / ((exp_win * battles)/battles)) * 100

    r_winc  = max(0, (r_win - 0.71) / (1-0.71))
    r_dmgc  = max(0, (r_dmg - 0.22) / (1-0.22))
    r_fragc = max(0, min(r_dmgc+0.2, (r_frag - 0.12) / (1-0.12)))
    r_spotc = max(0, min(r_dmgc+0.1, (r_spot - 0.38) / (1-0.38)))
    r_defc  = max(0, min(r_dmgc+0.1, (r_def - 0.10)  / (1-0.10)))

    wn8 = (980*r_dmgc + 210*r_dmgc*r_fragc + 155*r_fragc*r_spotc + 75*r_defc*r_fragc + 145*min(1.8,r_winc)).round(2)

    {
        'wn8'     => wn8,
        'r_winc'  => r_winc,
        'r_dmgc'  => r_dmgc,
        'r_fragc' => r_fragc,
        'r_spotc' => r_spotc
    }
  end

  def get_wn8(all_info, tanks_data, exp = @@exp )
    exp_dmg=0
    exp_spot=0
    exp_frag=0
    exp_def=0
    exp_win=0

    tanks_data.each do |value|
      exp_tank_data = get_tank_exp(value["tank_id"], exp)
      veh_battles  = value["statistics"]["battles"]
      exp_dmg  += veh_battles * exp_tank_data["expDamage"]
      exp_spot += veh_battles * exp_tank_data["expSpot"]
      exp_frag += veh_battles * exp_tank_data["expFrag"]
      exp_def  += veh_battles * exp_tank_data["expDef"]
      exp_win  += veh_battles * exp_tank_data["expWinRate"]

    end

    if !exp_dmg || !exp_spot || !exp_frag || !exp_def
      return 0
    end

    battles = all_info["statistics"]["all"]["battles"]
    r_dmg  = all_info["statistics"]["all"]["damage_dealt"] / exp_dmg
    r_spot = all_info["statistics"]["all"]["spotted"] / exp_spot
    r_frag = all_info["statistics"]["all"]["frags"] / exp_frag
    r_def  = all_info["statistics"]["all"]["dropped_capture_points"] / exp_def
    r_win  = (all_info["statistics"]["all"]["wins"] / ((exp_win * battles)/battles)) * 100


    r_winc  = max(0, (r_win - 0.71) / (1-0.71))
    r_dmgc  = max(0, (r_dmg - 0.22) / (1-0.22))
    r_fragc = max(0, min(r_dmgc+0.2, (r_frag - 0.12) / (1-0.12)))
    r_spotc = max(0, min(r_dmgc+0.1, (r_spot - 0.38) / (1-0.38)))
    r_defc  = max(0, min(r_dmgc+0.1, (r_def - 0.10)  / (1-0.10)))

    (980*r_dmgc + 210*r_dmgc*r_fragc + 155*r_fragc*r_spotc + 75*r_defc*r_fragc + 145*min(1.8,r_winc)).round(2)
  end

  def get_wn7(all_info, tanks_data, tankopedia = @@tankopedia)
    battles = all_info['statistics']['all']['battles'].to_f

    if battles.nil? || battles == 0
      return 0
    end

    frags = all_info['statistics']['all']['frags'] / battles
    damage = all_info['statistics']['all']['damage_dealt'] / battles
    spot = all_info['statistics']['all']['spotted'] / battles
    defrate = all_info['statistics']['all']['dropped_capture_points'] / battles
    winrate = all_info['statistics']['all']['wins'] / battles * 100

    weighted_tier = 0.00

    tanks_data.each do |value|
      #id = value['tank_id']
      battles_tank = value['statistics']['battles'].to_f
      tier = tankopedia["#{value['tank_id']}"]['level']
      weighted_tier += tier * battles_tank
    end

    tier = weighted_tier / battles

=begin
    puts "tier #{tier}"
    puts "frags #{frags}"
    puts "damage #{damage}"
    puts "spot #{spot}"
    puts "defrate #{defrate}"
    puts "winrate #{winrate}"
    puts "battles #{battles}"
=end


    wn7 = (1240 - 1040 / (min(tier, 6) ** 0.164)) * frags
    wn7 += damage * 530 / (184 * Math.exp((0.24 * tier)) + 130)
    wn7 += spot * 125 * min(tier, 3) / 3
    wn7 += min(defrate, 2.2) * 100
    wn7 += ((185 / (0.17 + Math.exp(((winrate - 35) * -0.134)))) - 500) * 0.45
    wn7 -= ((5 - min(tier, 5)) * 125) / (1 + Math.exp((tier - ((battles / 220) ** (3 / tier))) * 1.5))

    wn7.round(2)
  end

  def get_er(all_info, tanks_data, tankopedia = @@tankopedia)
    battles = all_info['statistics']['all']['battles'].to_f

    if battles.nil? || battles == 0
      return 0
    end

    frags = all_info['statistics']['all']['frags'] / battles
    damage = all_info['statistics']['all']['damage_dealt'] / battles
    spot = all_info['statistics']['all']['spotted'] / battles
    defrate = all_info['statistics']['all']['dropped_capture_points'] / battles
    cap = all_info['statistics']['all']['capture_points'] / battles
    winrate = all_info['statistics']['all']['wins'] / battles * 100

    weighted_tier = 0.00

    tanks_data.each do |value|
      #id = value['tank_id']
      battles_tank = value['statistics']['battles'].to_f
      tier = tankopedia["#{value['tank_id']}"]['level']
      weighted_tier += tier * battles_tank
    end

    tier = weighted_tier / battles

=begin
    puts "tier #{tier}"
    puts "frags #{frags}"
    puts "damage #{damage}"
    puts "spot #{spot}"
    puts "defrate #{defrate}"
    puts "winrate #{winrate}"
    puts "battles #{battles}"
=end


    er = damage * (10 / (tier + 2)) * (0.23 + 2* tier / 100)
    er += frags * 250
    er += spot * 150
    er += Math.log(cap + 1, 1.732)  * 150
    er += defrate * 150

    er.round(2)
  end

  def max(*values)
    values.max
  end

  def min(*values)
    values.min
  end

  def get_tank_exp(tank_id, exp = @@exp )
    res = {}
    exp.each do |a|
      if a['IDNum'] == tank_id
        res = {
            'expFrag' => a['expFrag'],
            'expDamage' => a['expDamage'],
            'expSpot' => a['expSpot'],
            'expDef' => a['expDef'],
            'expWinRate' => a['expWinRate']
        }
      end
    end
    res
  end
=begin
  def query
    query = "account/list/?application_id=#{APP_ID}&type=exact&search=#{@user}"
    "#{API_URI.path}#{query}"
  end
=end

  def exists?
    query = "account/list/?application_id=#{APP_ID}&type=exact&search=#{@user}"
    http_response = @@http.get("#{API_URI.path}#{query}")
    response = JSON.parse(http_response.body)

    if response['status'] == 'ok' && response['meta']['count'] > 0
      response['data'][0]['account_id']
    else
      nil
    end
  end

  def tankopedia
    return @@tankopedia
  end

  def get_short_info
    query = "account/info/?application_id=#{APP_ID}&account_id=#{@account_id}"
    http_response = @@http.get("#{API_URI.path}#{query}")
    response = JSON.parse(http_response.body)
    if response
      @user_name = response['data']["#{@account_id}"]['nickname']
      @clan_id = response['data']["#{@account_id}"]['clan_id']
      @all_info = response['data']["#{@account_id}"]

      if @clan_id
        query = "clans/info/?application_id=#{APP_ID}&clan_id=#{@clan_id}"
        http_response = @@http.get("/wgn/#{query}")
        response = JSON.parse(http_response.body)

        if response
          @clan_name = response['data']["#{@clan_id}"]['tag']
          @clan_emblem = response['data']["#{@clan_id}"]['emblems']['x64']['portal']
        end
      end

      if @clan_emblem.nil?
        @clan_emblem = '/img/logo-tanks.jpg'
      end
    end
  end

  def get_account_tech
    query = "account/tanks/?application_id=#{APP_ID}&account_id=#{@account_id}"
    http_response = @@http.get("#{API_URI.path}#{query}")
    response = JSON.parse(http_response.body)
    response['data']["#{@account_id}"]
  end

  def get_tankopedia
    query = "encyclopedia/tanks/?application_id=#{APP_ID}"
    http_response = @@http.get("#{API_URI.path}#{query}")
    response = JSON.parse(http_response.body)
    response['data']
  end

  def get_exp
    exp_url = "http://www.wnefficiency.net/exp/"
    exp_uri = URI.parse(exp_url)
    http = Net::HTTP.new(exp_uri.host, exp_uri.port)
    query = "expected_tank_values_latest.json"
    http_response = http.get("#{exp_uri.path}#{query}")
    response = JSON.parse(http_response.body)
    response['data']
  end

  def get_account_tech_stats
    query = "tanks/stats/?application_id=#{APP_ID}&account_id=#{@account_id}"
    http_response = @@http.get("#{API_URI.path}#{query}")
    response = JSON.parse(http_response.body)
    response['data']["#{@account_id}"]
  end

end
