class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  protect_from_forgery

  after_filter :set_csrf_cookie_for_ng

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  def execute_statement_sql(sql)
    results = ActiveRecord::Base.connection.execute(sql)
    if results.present?
      res = []
      results.each(:as => :hash) do |row|
        res << row
      end
      return res
    else
      return nil
    end
  end

  protected
    def verified_request?
      super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
    end

end
=begin


SELECT t.battles_count - t2.battles_count battles
,t.damage_dealt - t2.damage_dealt damage_dealt
,COALESCE(t.spotted - t2.spotted,0) spotted
,COALESCE(t.frags - t2.frags,0) frags
,COALESCE(t.dropped_capture_points - t2.dropped_capture_points,0) dropped_capture_points
,COALESCE(t.capture_points - t2.capture_points,0) capture_points
,COALESCE(t.wins_count - t2.wins_count,0) wins
,COALESCE(t.xp - t2.xp,0) xp
,COALESCE(t.survived_battles - t2.survived_battles,0) survived_battles
,xp.xp_max
FROM account_statistics t
JOIN account_statistics t2 ON t2.account_id = t.account_id
AND t2.on_date = DATE_ADD(CURDATE(), INTERVAL -1 day)
join (SELECT t.account_id, MAX(t.xp_max) xp_max
FROM account_statistics t
WHERE t.on_date <= CURDATE()
and t.on_date >= DATE_ADD(CURDATE(), INTERVAL -1 day)
GROUP by t.account_id) xp on xp.account_id = t.account_id
WHERE t.on_date = CURDATE()
AND COALESCE(t.battles_count - t2.battles_count, 0) != 0
and t.account_id = 2;



SELECT t.tank_id
,t.tank_lvl
,COALESCE(t.battles - t2.battles, 0) battles
FROM account_tanks t
JOIN account_tanks t2 ON t2.account_id = t.account_id
AND t.tank_id = t2.tank_id
AND t2.on_date = DATE_ADD(CURDATE(), INTERVAL -1 day)
WHERE t.on_date = CURDATE()
AND COALESCE(t.battles - t2.battles, 0) != 0
and t.account_id = 2;
=end
