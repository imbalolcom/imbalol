require 'UserApi'
class PagesController < ApplicationController

  def index
    if params[:session]
      #flash.now[:danger] = params[:session][:user]
      #flash.now[:danger] = 'Пользователь не найден.'
      user = UserApi.new(params[:session][:user])
      if user.account_id
        #redirect_to "/main/#{user.user}"
        redirect_to :action => 'main', :filter => user.user
      else
        flash.now[:danger] = 'Пользователь не найден.'
        #render 'index'
      end
    end
  end

  def main
    user = UserApi.new(params[:filter])

    if user.account_id
      user.get_short_info
      @user = user
=begin
      data = Account.where(account_id: user.account_id ).first
      first_date = DateTime.new(DateTime.now.year, DateTime.now.month, DateTime.now.day, 0,0,0,0)
      second_date = DateTime.new(DateTime.now.year, DateTime.now.month, DateTime.now.day, 23,59,59,59)
      @tanks = data.account_tanks.where(on_date: first_date..second_date)
=end
    else
      flash.now[:danger] = 'Пользователь не найден.'
      render 'index'
    end
    #render 'pages/index'
  end

  def tech
    user = UserApi.new(params[:filter])
    user.get_short_info
    @user = user
  end

  def info
    user = UserApi.new(params[:filter])
    user.get_short_info
    @user = user
  end

end
