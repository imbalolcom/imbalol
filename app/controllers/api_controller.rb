require 'UserApi'
class ApiController < ApplicationController
#rails server -b 0.0.0.0 -p 80
  def main
    user = UserApi.new(params[:filter]).info
    data = Account.where(account_id: user.account_id ).first
    first_date = DateTime.new(DateTime.now.year, DateTime.now.month, DateTime.now.day, 0,0,0,0)
    stat = data.account_statistics.where(on_date: first_date).first

    stat_1_day = user.get_stat_by_days
    stat_7_day = user.get_stat_by_days(7)

    main_table = []
    main_table << {'name' => 'Рейтинг эффективности', 'value' => stat.effencive_ratio,'value1' => stat_1_day['er'],'value7' => stat_7_day['er']}
    main_table << {'name' => 'WN8 (кпд)', 'value' => stat.wn8,'value1' => stat_1_day['wn8'],'value7' => stat_7_day['wn8']}
    main_table << {'name' => 'WN7', 'value' => stat.wn7,'value1' => stat_1_day['wn7'],'value7' => stat_7_day['wn7']}
    main_table << {'name' => 'Количество боев', 'value' => stat.battles_count, 'value1' => stat_1_day['battles'],'value7' => stat_7_day['battles']}

    main_table << {'name' => 'Процент побед',
                   'value' => "#{stat.wins_ratio.nil? ? nil :stat.wins_ratio.to_s + '%'}",
                   'value1' => "#{stat_1_day['wins_ratio'].nil? ? nil : stat_1_day['wins_ratio'].to_s+'%'}",
                   'value7' => "#{stat_7_day['wins_ratio'].nil? ? nil : stat_7_day['wins_ratio'].to_s+'%'}"
    }

    main_table << {'name' => 'Средний урон', 'value' => stat.damage_avg,'value1' => stat_1_day['damage_avg'],'value7' => stat_7_day['damage_avg']}
    main_table << {'name' => 'Средний опыт', 'value' => stat.xp_avg,'value1' => stat_1_day['xp_avg'],'value7' => stat_7_day['xp_avg']}
    main_table << {'name' => 'Максимальный опыт за бой', 'value' => stat.xp_max, 'value1' => stat_1_day['xp_max'],'value7' => stat_7_day['xp_max']}
    main_table << {'name' => 'Уничтожено врагов', 'value' => stat.frage_avg, 'value1' => stat_1_day['frage_avg'],'value7' => stat_7_day['frage_avg']}
    main_table << {'name' => 'Выжил в битвах',
                   'value' => "#{stat.survived_ratio.nil? ? nil : stat.survived_ratio.to_s+'%'}",
                   'value1' => "#{stat_1_day['survived_ratio'].nil? ? nil : stat_1_day['survived_ratio'].to_s+'%'}",
                   'value7' => "#{stat_7_day['survived_ratio'].nil? ? nil : stat_7_day['survived_ratio'].to_s+'%'}"
    }

    stat_all = data.account_statistics.order(:on_date)
    all_charts = {}
    arr_dmg = []
    arr_xp = []
    arr_wn8 = []
    arr_wn7 = []
    arr_er = []
    if stat_all.length == 1
      all_charts = {
          'mainChart' => {'name'=> 'Рейтинг эффективности', 'value' => stat_all.first.effencive_ratio.to_i, 'values'=> [[0.to_f,0.to_f],[stat_all.first.battles_count.to_f,stat_all.first.effencive_ratio]]},
          'win8Chart' => {'name'=> 'WN8', 'value' =>  stat_all.first.wn8, 'values' => [0, stat_all.first.wn8]},
          'win7Chart' => {'name'=> 'WN7', 'value' =>  stat_all.first.wn7, 'values' => [0, stat_all.first.wn7]},
          'damageChart' => {'name'=> 'Средний урон', 'value' =>  stat_all.first.damage_avg, 'values' => [0, stat_all.first.damage_avg]},
          'xpChart' => {'name'=> 'Средний опыт', 'value' =>  stat_all.first.xp_avg, 'values' => [0, stat_all.first.xp_avg]}
      }
    else
      min_v = 100000
      max_v = 0
      stat_all.each do |value|
        arr_dmg << value.damage_avg
        arr_xp << value.xp_avg
        arr_wn8 << value.wn8
        arr_wn7 << value.wn7
        arr_er << [value.battles_count.to_f, value.effencive_ratio.to_i]

        max_v = value.effencive_ratio if value.effencive_ratio > max_v
        min_v = value.effencive_ratio if value.effencive_ratio < min_v
        if min_v == max_v
          max_v = min_v + 5
          min_v = min_v - 5
        end

        if max_v - min_v < 5
          max_v = min_v + 5
        end
        if min_v > 5
          min_v = min_v - 5
        end
      end

      all_charts = {
          'mainChart' => {'name'=> 'Рейтинг эффективности', 'value' => stat_all.last.effencive_ratio.to_i, 'values'=> arr_er, 'min' => min_v, 'max' => max_v},
          'win8Chart' => {'name'=> 'WN8', 'value' =>  stat_all.last.wn8, 'values' => arr_wn8},
          'win7Chart' => {'name'=> 'WN7', 'value' =>  stat_all.last.wn7, 'values' => arr_wn7},
          'damageChart' => {'name'=> 'Средний урон', 'value' =>  stat_all.last.damage_avg, 'values' => arr_dmg},
          'xpChart' => {'name'=> 'Средний опыт', 'value' =>  stat_all.last.xp_avg, 'values' => arr_xp}
      }
    end
    stat_tanks = data.account_tanks.where(on_date: first_date).select(:tank_nation, "sum(battles) as sum_battles").group(:tank_nation)

    i = 0.0
    nations_pie = []
    stat_tanks.each do |stat|
      if stat.sum_battles > 5
        i += 0.1
        nations_pie << {'data' => stat.sum_battles, 'color' => "rgba(255,255,255,#{i})", 'label' => stat.tank_nation}
      end
    end
    stat_tanks = data.account_tanks.where(on_date: first_date).select(:tank_type, "sum(battles) as sum_battles").group(:tank_type)
    i = 0.0
    models_pie = []
    stat_tanks.each do |stat|
      if stat.sum_battles > 5
        i += 0.1
        models_pie << {'data' => stat.sum_battles, 'color' => "rgba(255,255,255,#{i})", 'label' => stat.tank_type}
      end
    end

    result = {
        'mainTable' => main_table,
        'charts' => all_charts,
        'pieCharts' => {'countries' => nations_pie, 'models' => models_pie}
    }


    render json: result
  end

  def tech
    user = UserApi.new(params[:filter])
    data = Account.where(account_id: user.account_id ).first
    first_date = DateTime.new(DateTime.now.year, DateTime.now.month, DateTime.now.day, 0,0,0,0)
    tanks = data.account_tanks.where(on_date: first_date)
    result = []
    tanks.each do |tank|
      exp_tank_data = user.get_tank_exp(tank.tank_id)
      veh_battles = tank.battles.to_f
      exp_dmg  = (exp_tank_data["expDamage"]).round(2)
      exp_spot = (exp_tank_data["expSpot"]).round(2)
      exp_frag = (exp_tank_data["expFrag"]).round(2)
      exp_win  = (exp_tank_data["expWinRate"]).round(2)

      avg_dmg  = (tank.damage_dealt / veh_battles).round(2) unless veh_battles.nil? || veh_battles.zero?
      avg_spot = (tank.spotted / veh_battles).round(2)  unless veh_battles.nil? || veh_battles.zero?
      avg_frag = (tank.frags / veh_battles).round(2) unless veh_battles.nil? || veh_battles.zero?
      avg_win  = (tank.wins / veh_battles * 100).round(2) unless veh_battles.nil? || veh_battles.zero?
      result << {
          'name' => tank.tank_name,
          'type' => tank.tank_type,
          'nation' => tank.tank_nation,
          'lvl' => tank.tank_lvl,
          'battles' => tank.battles,
          'win_rate' => tank.win_rate.to_f,
          'wn8' => tank.wn8.round(),
          'dmg' => {'value' => tank.r_dmg_c.to_f, 'tooltip' => "#{avg_dmg}/#{exp_dmg} avgDmg/expDmg"},
          'wr' => {'value' => tank.r_win_c.to_f, 'tooltip' => "#{avg_win}%/#{exp_win}% avgWin/expWin"},
          'spot' => {'value' => tank.r_spot_c.to_f, 'tooltip' => "#{avg_spot}/#{exp_spot} avgSpot/expSpot"},
          'frag' => {'value' => tank.r_frag_c.to_f, 'tooltip' => "#{avg_frag}/#{exp_frag} avgFrag/expFrag"},
          'show' => 'true'
      }
    end

    render json: result
  end

  def feedback
    UserMailer.feedback_email(params[:message], params[:email]).deliver_now
    render text: "sended"
  end

  def all_exp
    res = []
    user = UserApi.new(params[:filter])
    tankopedia = user.tankopedia
    #exp = user.get_exp
    tankopedia.each do |tank|
      exp_tank_data = user.get_tank_exp(tank[0].to_i)
      exp_dmg  = exp_tank_data["expDamage"]
      exp_spot = exp_tank_data["expSpot"]
      exp_frag = exp_tank_data["expFrag"]
      exp_def  = exp_tank_data["expDef"]
      exp_win  = exp_tank_data["expWinRate"]
      res << {
          name: tank[1]['name_i18n'],
          type: tank[1]['type_i18n'],
          nation: tank[1]['nation_i18n'],
          level: tank[1]['level'],
          image: tank[1]['image_small'],
          exp_dmg: exp_tank_data["expDamage"],
          exp_spot: exp_tank_data["expSpot"],
          exp_frag: exp_tank_data["expFrag"],
          exp_def: exp_tank_data["expDef"],
          exp_win: exp_tank_data["expWinRate"]
      }
    end
    render json: res
  end


  def all_tanks
    res = []
    user = UserApi.new(params[:filter])
    tankopedia = user.tankopedia
    tankopedia.each { |tank| res << {id: tank[0].to_i, value: tank[1]['name_i18n']}}
    render json: res
  end

  def wn8_tank
    user = UserApi.new(params[:filter])
    #exp = user.get_exp
    value = {'tank_id' => params[:id].to_i, 'all' => {
        'battles' =>params[:battles].to_f,
        'damage_dealt' =>params[:damage_dealt].to_f,
        'spotted' =>params[:spotted].to_f,
        'frags' =>params[:frags].to_f,
        'dropped_capture_points' =>params[:dropped_capture_points].to_f,
        'wins' =>params[:wins].to_f
    } }

    render json: user.get_wn8_tank(value)
  end

  def sign_preview
    params[:nickname] ||= "Nickname"
    params[:color] ||= "ffffff"
    params[:clan] ||= "Clan"

    params[:nickname] = " " if params[:nickname] == ''
    params[:clan] = " " if params[:clan] == ''
    params[:color] = "ffffff" if params[:color] == ''

    background = generate_sign(params)

    send_data background.to_blob, :type => 'image/png', :disposition => 'inline'
  end

  def sign_show
    sign = SignImage.find(params[:id])

    send_data sign.data, :type => 'image/png', :filename => "#{sign.name}.png", :disposition => 'inline'

  end

  def sign_save
    params[:nickname] ||= "Nickname"
    params[:color] ||= "ffffff"
    params[:clan] ||= "Clan"

    params[:nickname] = " " if params[:nickname] == ''
    params[:clan] = " " if params[:clan] == ''
    params[:color] = "ffffff" if params[:color] == ''  

    background = generate_sign(params)

    sign = SignImage.new(name: params[:nickname], data: background.to_blob )

    if sign.save
      path = url_for :controller => 'api', :action =>'sign_show', :id => sign.id
      forum = "[url=#{root_url}][img]#{path}[/img][/url]"
      html = "<a href='#{root_url}' target='blank'><img src='#{path}' border='0'></a>"
      render json: { image: path, url: root_url, forum: forum, html: html}
    else
      render json: {image: "error"}
    end
    
  end

  def generate_sign(params)
    require 'rmagick'
    background = Magick::Image.read(File.join(Rails.root, 'app', 'assets', 'images', 'sing_background_min.png')).first
    background = background.resize_to_fill(450,160).quantize(256, Magick::GRAYColorspace).contrast(true)

    gc = Magick::Draw.new
    gc.stroke('transparent')
    gc.fill("##{params[:color]}")
    gc.pointsize('35')
    gc.font_family = "Times"
    gc.font_weight = Magick::BoldWeight
    gc.font_style = Magick::NormalStyle
    gc.text(x = 30, y = 50, text = params[:nickname])
    gc.draw(background)
    ##########
    gc.pointsize('20')
    gc.text(x = 30, y = 75, text = params[:clan])
    gc.draw(background)
    #resize_to_fill(450,160).
    background = background.quantize(256, Magick::RGBColorspace).contrast(true)
    #background.composite!(background.negate,0,0, Magick::CopyOpacityCompositeOp)

    background
  end
end
