class UserMailer < ApplicationMailer
  default from: 'info@imbalol.ru'

  #UserMailer.feedback_email("123", "qwe@adasda.es").deliver_now
  def feedback_email(message, email)
    @message = message
    @mail = email
    @url  = 'http://imbalol.ru'
    mail(to: 'r.konstantinov@energos.perm.ru', subject: 'Отзыв о сайте imbalol.ru')
  end
end
