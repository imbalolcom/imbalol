class ApplicationMailer < ActionMailer::Base
  default from: "info@imbalol.ru"
  layout 'mailer'
end
