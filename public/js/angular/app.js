'use strict';


var app = angular.module('mainApp', ['ngRoute']);

app.directive('selectWatcher', function ($timeout) {
    return {
        link: function (scope, element, attr) {
            var last = attr.last;
            if (last === "true") {
                $timeout(function () {
                    //$(element).parent().selectpicker('val', 'any');
                    $(element).parent().selectpicker('refresh');
                });
            }
        }
    };
});

app.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider.when('/main/', {
        templateUrl: 'templates/login.html',
        controller: 'SearchCtrl'  
    });

    $routeProvider.when('/main/:filter', {
        templateUrl: '/templates/userdata.html',
        controller: 'UserDataCtrl'  
    });

    $routeProvider.when('/main/:filter/tech', {
        templateUrl: '/templates/tech.html',
        controller: 'UserTechCtrl'  
    });    
    $routeProvider.when('/main/:filter/info', {
        templateUrl: '/templates/info.html',
        controller: 'InfoCtrl'  
    });
    $routeProvider.when('/main/:filter/calc', {
        templateUrl: '/templates/calc.html',
        controller: 'CalcCtrl'
    });

    $routeProvider.when('/main/:filter/sign', {
        templateUrl: '/templates/sign.html',
        controller: 'SignCtrl'
    });

    $routeProvider.otherwise({
        controller: function(){
            window.location.replace('/');
        }
        //redirectTo: '/'
    });

    if(window.history && window.history.pushState){
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, PUT, POST';
}]); 


app.run(['$rootScope', '$location', function($rootScope, $location){
    $rootScope.user = { 
        name : null,
        loginFormInfo : null
    };

    $rootScope.toStart = function() {
        window.location.replace('/');
    }
    /*if (typeof($rootScope.user) === 'undefined' )
    {
        $location.path('/');
    };*/ 
    $('body').on('click touchstart', '#menu-toggle', function(e){
        e.preventDefault();
        $('html').toggleClass('menu-active');
        $('#sidebar').toggleClass('toggled');
        //$('#content').toggleClass('m-0');
    }); 
  
    $('body').on('click touchstart', '.drawer-toggle', function(e){
        e.preventDefault();
        var drawer = $(this).attr('data-drawer');

        $('.drawer:not("#'+drawer+'")').removeClass('toggled');

        if ($('#'+drawer).hasClass('toggled')) {
            $('#'+drawer).removeClass('toggled');
        }
        else{
            $('#'+drawer).addClass('toggled');
        }
    });

    //Close when click outside
    $(document).on('mouseup touchstart', function (e) {
        var container = $('.drawer, .tm-icon');
        if (container.has(e.target).length === 0) {
            $('.drawer').removeClass('toggled');
            $('.drawer-toggle').removeClass('open');
        }
    });

    //Close
    $('body').on('click touchstart', '.drawer-close', function(){
        $(this).closest('.drawer').removeClass('toggled');
        $('.drawer-toggle').removeClass('open');
    });
    wLoad();
    dLoad();

}]);


app.controller('MenuController', function ($scope, $rootScope, $location, $routeParams, $q, FindData) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
});



app.controller('FeedbackController', function ($scope, $rootScope, $location, $routeParams, $q, FindData) {
    $scope.feedback = {};
    $scope.feedback.message = null;
    $scope.feedback.email = null;
    $scope.sendFeedback = function(){
        if ($scope.feedback.message !== null && $scope.feedback.email !== null) {
            if ($scope.validateEmail($scope.feedback.email)) {
                FindData.sendFeddback($scope.feedback).then(function(data){
                    $scope.feedback.formInfo = "Успешно отправлено! Спасибо за Ваше мнение!";
                    $scope.feedback.message = null;
                    $scope.feedback.email = null;
                });
            }
            else {
                $scope.feedback.formInfo = "Email не соответствует формату!";
            }
        }
        else {
            $scope.feedback.formInfo = "Вы не написали сообщение!";
        }
    };
    $scope.validateEmail = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
});