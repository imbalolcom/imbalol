app.controller('CalcCtrl', function ($scope, $rootScope, $location, $routeParams, $q, FindData) {
    $('.select').selectpicker();
    $scope.tankTable = null;
    $scope.error = null;
    FindData.allTanks().then(function(data){
        hideLoader();
        $scope.tankTable = data;
        //$('.select').selectpicker('refresh');
    });

    $scope.tank = {}
    $scope.tank.id = 0;
    $scope.result = {};
    //$scope.tank.data = null;
    $scope.loadStats = function(){
        $scope.tank.battles = 1;
        $scope.tank.wins = 1;
        $scope.tank.frags = 1;
        $scope.tank.damage_dealt = 1000;
        $scope.tank.spotted = 1.5;
        $scope.tank.dropped_capture_points = 1.5;
        //showLoader();
/*        FindData.allTanks().then(function(data){
            hideLoader();
            $scope.tank.data = data;
        });*/
        $('#resultFrame').css('display', 'none');
        $('#errorFrame').css('display', 'none');
    }

    $scope.calculate = function(){
        //console.log($scope.tank);
        showLoader();
        FindData.getWn8Tank($scope.tank).then(function(data){
            hideLoader();
            $scope.result = data;
            $('#resultFrame').css('display', 'block');
        }, function(error){
            hideLoader();
            $('#errorFrame').css('display', 'block');
            $scope.error = 'Выберите танк и заполните значения!';
        });
    }
})