
app.controller('InfoCtrl', function ($scope, $rootScope, $location, $routeParams, $q, FindData) {
    
    $('#jumbotron').hide();
    var page = $('#p-load');

    FindData.expTanks().then(function(data){
        //var page = $('#p-load');
        page.animate({opacity:'0'},1000, function(){
            page.hide();
        });
        $scope.expTable = data;
    });

    $scope.toStart = function(){
        $location.path('/');
    };

    $scope.toMain = function(){
        $location.path('/'+ $routeParams.filter);
    };  
    $scope.toTech = function(){
        $location.path('/'+ $routeParams.filter + '/tech');
    };    
    $scope.toInfo = function(){
        $location.path('/'+ $routeParams.filter + '/info');
    }; 

});


