
app.controller('UserDataCtrl', function ($scope, $rootScope, $location, $routeParams,$timeout, $q, FindData) {

    $('#jumbotron').hide();
    var exp = {};
    var tanks = {};
    $scope.charts = {};
    //FindData.clear();
    var page = $('#p-load');

    //console.log($routeParams.filter);

    FindData.mainPage($routeParams.filter).then(function(data){

        page.animate({opacity:'0'},1000, function(){
           page.hide();
        });

        if (data.account_id !== -1){
            $scope.mainTable = data.mainTable;
            $scope.charts = data.charts;

            lineChart($scope.charts.win8Chart.values, "stats-line-2");
            lineChart($scope.charts.win7Chart.values, "stats-line-3");
            lineChart($scope.charts.damageChart.values, "stats-line-4");
            lineChart($scope.charts.xpChart.values, "stats-line");  

            viewMainChart($scope.charts.mainChart.values, $scope.charts.mainChart.min, $scope.charts.mainChart.max); 

            viewPieChart('#pie-chart2',data.pieCharts.countries);
            viewPieChart('#pie-chart1',data.pieCharts.models);

            $timeout(function() {
                //animation charts titles
                ChartQuickStat();
            }, 100); 
        }
    });

    /*FindData.getUserMainData($routeParams.filter).then(function(data){
        if (data.account_id == -1){
            $rootScope.user = { 
                name : $routeParams.filter,
                loginFormInfo : "Пользователь не найден"
            };
            $rootScope.toStart();
        }
        if (data.emblem == null){
            data.emblem = '/img/logo-tanks.jpg';
        }
        $scope.user = data;
        //wLoad();
        //dLoad();
    });*/

    $scope.user = {};
    var statArr = [];

    $scope.mainTable = [];

    $scope.toStart = function(){
        window.location.replace('/');
        //$location.path('/');
    };
    $scope.toTech = function(){
        $location.path('/'+ $routeParams.filter + '/tech');
    };   
    $scope.toInfo = function(){
        $location.path('/'+ $routeParams.filter + '/info');
    };   

    /*$scope.$on('$viewContentLoaded',function(){
        //mainChart();
        wLoad();
        dLoad();        
    });*/

    var lineChart = function(values, id){
        $("#" + id).sparkline(values, {
            type: 'line',
            height: '65',
            width: '100%',
            lineColor: 'rgba(255,255,255,0.4)',
            lineWidth: 1.25,
            fillColor: 'rgba(0,0,0,0.2)',
            barWidth: 5,
            barColor: '#C5CED6',

        });        
    };

    var viewPieChart = function(elem, pieData){
        if($(elem)[0]){
            $.plot(elem, pieData, {
                series: {
                    pie: {
                        show: true,
                        stroke: { 
                            width: 0,
                            
                        },
                        label: {
                            show: true,
                            radius: 3/4,
                            formatter: function(label, series){
                                return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
                            }, 
                            background: { 
                                opacity: 0.5,
                                color: '#000'
                            }
                        }
                    }
                }
            });
        }
    }

    var viewMainChart = function(data, min, max){
        var d1 = data; //[[1,14], [2,15], [3,18], [4,16], [5,19], [6,17], [7,15], [8,16], [9,20], [10,16], [11,18]];

        $.plot('#line-chart', [ {
            data: d1,
            label: "На",

        },],

            {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 1,
                        fill: 0.25,
                    },

                    color: 'rgba(255,255,255,0.7)',
                    shadowSize: 0,
                    points: {
                        show: true,
                    }
                },

                yaxis: {
                    min: min,
                    max: max,
                    tickColor: 'rgba(255,255,255,0.15)',
                    tickDecimals: 0,
                    font :{
                        lineHeight: 13,
                        style: "normal",
                        color: "rgba(255,255,255,0.8)",
                    },
                    shadowSize: 0,
                },
                xaxis: {
                    tickColor: 'rgba(255,255,255,0)',
                    tickDecimals: 0,
                    font :{
                        lineHeight: 13,
                        style: "normal",
                        color: "rgba(255,255,255,0.8)",
                    }
                },
                grid: {
                    borderWidth: 1,
                    borderColor: 'rgba(255,255,255,0.25)',
                    labelMargin:10,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius:6,
                },
                legend: {
                    show: false
                }
            });

        $("#line-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(0);
                $("#linechart-tooltip").html(item.series.label + " " + x + " битвы = " + y).css({top: item.pageY+5, left: item.pageX-35}).fadeIn(200);
            }
            else {
                $("#linechart-tooltip").hide();
            }
        });

        $("<div id='linechart-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }
}); 

