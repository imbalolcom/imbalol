
app.controller('UserTechCtrl', function ($scope, $rootScope, $location, $routeParams, $q, FindData) {

    $('#jumbotron').hide();
    var page = $('#p-load');

    $scope.toStart = function(){
        $location.path('/');
    };
    $scope.checkBattles = true;

    $scope.toMain = function(){
        $location.path('/'+ $routeParams.filter);
    };  
    $scope.toInfo = function(){
        $location.path('/'+ $routeParams.filter + '/info');
    };
    $scope.feedback = {};
    $scope.feedback.message = null;

    $scope.cntBattlesFilter = function (item) {
        return !(!$scope.checkBattles && item.battles < 100);
    };

    $scope.predicate = 'wn8';
    $scope.reverse = true;
    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    FindData.techPage($routeParams.filter).then(function(data){
        //var page = $('#p-load');
        page.animate({opacity:'0'},1000, function(){
             page.hide();
        });           
        $scope.mainTable = data;

      
               
    });

});

