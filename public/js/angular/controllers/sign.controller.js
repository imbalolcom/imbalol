app.controller('SignCtrl', function ($scope, $rootScope, $location, $routeParams, $q, FindData) {
    $scope.sign = {}
    $scope.sign.image = '/api/sign';
    $scope.sign.nickname = 'Nickname';
    $scope.sign.clan = 'Clan';
    $scope.sign.color = '#ffffff';

    $scope.result = {}
    $scope.result.image = null;
    $scope.result.url = null;

    //console.log($scope.sign);

    if($('.color-picker')[0]) {
        $('.color-picker').colorpicker({
            color: $scope.color,
            onChange: function(hsb,hex, rgb){
                $scope.sign.color = hex;
            }
        });
    }

    /*$('#colorValue').on('change', function(){
        $scope.sign.color = $('#colorValue').val();
        $scope.loadSign();
    }); */

    if($('[class*="color-picker"]')[0]) {
        $('[class*="color-picker"]').colorpicker().on('changeColor', function(e){
            var colorThis = $(this).val();
            $(this).closest('.color-pick').find('.color-preview').css('background',e.color.toHex());
            //$('#colorValue').val(e.color.toHex());
            $scope.sign.color = e.color.toHex();
            $scope.$apply();
            //$scope.sign.image = '/api/sign?nickname=' + $scope.sign.nickname + '&color=' + $scope.sign.color;
            $scope.loadSign();
        });

    };

    hideLoader();
    

    $scope.error = null;

    $scope.loadSign = function(){
        //showLoader();
        
        $('#errorFrame').css('display', 'none');
        $scope.sign.image = '/api/sign?nickname=' + $scope.sign.nickname + '&color=' + $scope.sign.color.replace('#', '') + '&clan=' + $scope.sign.clan;
        //console.log($scope.sign);

    }

    $scope.saveSign = function(){
        showLoader();
        FindData.saveSign($scope.sign).then(function(data){
            hideLoader();
            $scope.result = data;
            $('#resultFrame').css('display', 'block');
            $('#generateSign').css('display', 'none');
        }, function(error){
            hideLoader();
            $('#errorFrame').css('display', 'block');
            $scope.error = 'Произошла ошибка...';
        }); 

    }
})