
app.controller('SearchCtrl', function ($scope, $rootScope, $location, $routeParams, FindData) {
    $('#jumbotron').show();
    $scope.user = {
        name: $rootScope.user.name,
        formInfo: $rootScope.user.loginFormInfo
    }

    $scope.findUser = function(){
        $scope.user.formInfo = "";
        $rootScope.user.name = this.user.name;
        FindData.searchUser();
    }

    $rootScope.$on("user:finded", function(){
        $scope.changeRoute('#/' + $rootScope.user.name);
        //console.log($rootScope.user.account_id);
    })

    $rootScope.$on("user:notfound", function(){
        $scope.user.formInfo = "Пользователь не найден";
    })    

    $scope.changeRoute = function(url, forceReload) {
        $scope = $scope || angular.element(document).scope();
        if(forceReload || $scope.$$phase) { // that's right TWO dollar signs: $$phase
            window.location = url;
        } else {
            $location.path(url);
            $scope.$apply();
        }
    };

}); 

