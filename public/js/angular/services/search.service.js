app.factory('FindData', function ($http, $rootScope, $q, $location) {

    //var baseApiUrl = "http://api.worldoftanks.ru";

    var user = {
      /*  name: "",
        account_id: null,
        clan_id: null,
        clan_name: "",
        battles_count: 0,
        xp_avg: 0,
        damage_avg: 0,
        wins_ratio: 0 */
    }; 

    var statistics = [];

    var clear = function(){
        statistics.length = 0;
    }

    var mainPage = function(userName) {

        var deferred = $q.defer();
        var rUrl = "/api/main/" + userName;
        $http({ method: 'GET', 
                url: rUrl
             })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);  
        });  

        return deferred.promise;    
    };  

    var techPage = function(userName) {

        var deferred = $q.defer();
        var rUrl = "/api/tech/" + userName;
        $http({ method: 'GET', 
                url: rUrl
             })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);  
        });  

        return deferred.promise;    
    };

    var expTanks = function() {

        var deferred = $q.defer();
        var rUrl = "/api/expinfo";
        $http({ method: 'GET',
            url: rUrl
        })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
            });

        return deferred.promise;
    };

    var allTanks = function() {
        var deferred = $q.defer();
        var rUrl = "/api/alltanks";
        $http({ method: 'GET',
            url: rUrl
        })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
            });

        return deferred.promise;
    };

    var getWn8Tank = function(data) {
        var deferred = $q.defer();
        var rUrl = "/api/wn8tank";
        $http({ method: 'POST',
            url: rUrl,
            data: data
        })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
            });

        return deferred.promise;
    };

    var saveSign = function(data) {
        var deferred = $q.defer();
        var rUrl = '/api/sign_save?nickname=' + data.nickname + '&color=' + data.color.replace('#', '') + '&clan=' + data.clan;
        $http({ method: 'GET',
            url: rUrl
            /*,data: data */
        })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
            });

        return deferred.promise;
    };    

    var sendFeddback = function(message) {
        var deferred = $q.defer();
        var datap = {
            message: message
        };

        var rUrl = "/api/feedback";
        $http({ method: 'POST',
                url: rUrl,
                //data: $.param(datap),
                data: message //,
                //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
             })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);                      
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
        });

        return deferred.promise;       
    };
    return {
        mainPage: mainPage,
        user : user,
        //getStatistics : statistics,
        techPage : techPage,
        sendFeddback : sendFeddback,
        expTanks : expTanks,
        allTanks: allTanks,
        getWn8Tank : getWn8Tank,
        saveSign : saveSign,
/*        searchUser : function () {
            var rUrl = "http://imbalol.ru/api/api.php?method=userInfo&account_id=" + $rootScope.user.name;
            $http({ method: 'GET', 
                    url: rUrl,
                 })
                .success(function (data, status, headers, config) {
                    if (data.account_id != -1)
                    {
                        $rootScope.user.account_id = data.account_id
                        $rootScope.$broadcast('user:finded', data);
                    }
                    else
                    {
                        $rootScope.$broadcast('user:notfound', data);
                    }
                })
                .error(function (data, status, headers, config) {
                    $rootScope.$broadcast('error', data);
            });
        },

        getUserMainData : function (userName) {

            var deferred = $q.defer();
            var rUrl = "http://imbalol.ru/api/api.php?method=userInfo&account_id=" + userName;
            $http({ method: 'GET',
                    url: rUrl
                 })
                .success(function (data, status, headers, config) {
                    $rootScope.user.name = userName;
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
            });

            return deferred.promise;
        },*/

        //loadStatictics : loadStaticticsAll,
        //loadStaticticsByType : getStatisticsOnDate,
        //getTechincsAll : getTechincsAll,
        //loadExp : loadExp,
        clear : clear

    }
});