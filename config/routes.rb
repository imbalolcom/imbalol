Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'pages#index'

  get '/main/:filter' => 'pages#main', as: :main
  get '/main/:filter/tech' => 'pages#main', as: :tech_angular
  get '/main/:filter/info' => 'pages#main', as: :info_angular
  get '/main/:filter/calc' => 'pages#main', as: :calc_angular
  get '/main/:filter/sign' => 'pages#main', as: :sign_angular

  get '/tech/:filter' => 'pages#tech', as: :tech
  get '/info/:filter' => 'pages#info', as: :info

  get '/api/main/:filter' => 'api#main', as: :api_main
  get '/api/tech/:filter' => 'api#tech', as: :api_tech
  post '/api/feedback' => 'api#feedback', as: :api_feedback
  get '/api/expinfo' => 'api#all_exp', as: :api_all_exp
  get '/api/alltanks' => 'api#all_tanks', as: :api_all_tanks
  post '/api/wn8tank' => 'api#wn8_tank', as: :api_wn8_tank

  get '/api/sign' => 'api#sign_preview', as: :api_sign_preview
  get '/api/sign_save' => 'api#sign_save', as: :api_sign_save
  get '/api/sign/:id' => 'api#sign_show', as: :api_sign_show

  match '*path' => 'pages#index', via: [:get, :post]
  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
