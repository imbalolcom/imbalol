# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151130095246) do

  create_table "account_statistics", force: :cascade do |t|
    t.integer  "account_id",             limit: 4
    t.date     "on_date",                                                   null: false
    t.decimal  "effencive_ratio",                  precision: 10, scale: 2
    t.decimal  "wn8",                              precision: 10, scale: 2
    t.decimal  "wn7",                              precision: 10, scale: 2
    t.integer  "battles_count",          limit: 4
    t.integer  "wins_count",             limit: 4
    t.decimal  "damage_dealt",                     precision: 10, scale: 2
    t.decimal  "survived_battles",                 precision: 10, scale: 2
    t.decimal  "frags",                            precision: 10, scale: 2
    t.decimal  "wins_ratio",                       precision: 10, scale: 2
    t.decimal  "damage_avg",                       precision: 10, scale: 2
    t.decimal  "xp_avg",                           precision: 10, scale: 2
    t.decimal  "xp_max",                           precision: 10, scale: 2
    t.decimal  "frage_avg",                        precision: 10, scale: 2
    t.decimal  "survived_ratio",                   precision: 10, scale: 2
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "xp",                     limit: 4
    t.decimal  "dropped_capture_points",           precision: 12, scale: 2
    t.decimal  "spotted",                          precision: 12, scale: 2
    t.decimal  "capture_points",                   precision: 12, scale: 2
  end

  create_table "account_tanks", force: :cascade do |t|
    t.integer  "tank_id",                limit: 4,                             null: false
    t.string   "tank_name",              limit: 1000,                          null: false
    t.string   "tank_type",              limit: 1000,                          null: false
    t.string   "tank_nation",            limit: 1000,                          null: false
    t.integer  "tank_lvl",               limit: 4,                             null: false
    t.integer  "account_id",             limit: 4
    t.date     "on_date",                                                      null: false
    t.integer  "battles",                limit: 4
    t.integer  "wins",                   limit: 4
    t.decimal  "damage_dealt",                        precision: 10, scale: 2
    t.decimal  "spotted",                             precision: 10, scale: 2
    t.decimal  "frags",                               precision: 10, scale: 2
    t.decimal  "dropped_capture_points",              precision: 10, scale: 2
    t.decimal  "win_rate",                            precision: 10, scale: 2
    t.decimal  "wn8",                                 precision: 10, scale: 2
    t.decimal  "r_dmg_c",                             precision: 10, scale: 2
    t.decimal  "r_win_c",                             precision: 10, scale: 2
    t.decimal  "r_spot_c",                            precision: 10, scale: 2
    t.decimal  "r_frag_c",                            precision: 10, scale: 2
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "account_id", limit: 4,    null: false
    t.string   "user_name",  limit: 1000, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "sign_images", force: :cascade do |t|
    t.string   "name",       limit: 255,        null: false
    t.binary   "data",       limit: 4294967295, null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "test_models", force: :cascade do |t|
    t.string   "test",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
