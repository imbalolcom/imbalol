class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :account_id,    null: false
      t.string  :user_name,     limit: 1000,   null: false
      t.timestamps null: false
    end
  end
end
