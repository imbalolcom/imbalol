class CreateAccountTanks < ActiveRecord::Migration
  def change
    create_table :account_tanks do |t|
      t.integer   :tank_id,                  null: false
      t.string    :tank_name,                limit: 1000,   null: false
      t.string    :tank_type,                limit: 1000,   null: false
      t.string    :tank_nation,              limit: 1000,   null: false
      t.integer   :tank_lvl,                 null: false
      t.references :account
      t.date    :on_date,                    null: false

      t.integer :battles
      t.integer :wins
      t.decimal :damage_dealt,               precision: 10, scale: 2
      t.decimal :spotted,                    precision: 10, scale: 2
      t.decimal :frags,                      precision: 10, scale: 2
      t.decimal :dropped_capture_points,     precision: 10, scale: 2
      t.decimal :win_rate,                   precision: 10, scale: 2

      t.decimal :wn8,                        precision: 10, scale: 2

      t.decimal :r_dmg_c,                    precision: 10, scale: 2
      t.decimal :r_win_c,                    precision: 10, scale: 2
      t.decimal :r_spot_c,                   precision: 10, scale: 2
      t.decimal :r_frag_c,                   precision: 10, scale: 2

      t.timestamps null: false
    end
  end
end
