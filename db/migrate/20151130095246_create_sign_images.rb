class CreateSignImages < ActiveRecord::Migration
  def change
    create_table :sign_images do |t|

      t.string :name, null: false
      t.binary :data, null: false, limit: 10.megabyte
      t.timestamps null: false
    end
  end
end
