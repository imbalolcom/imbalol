class CreateAccountStatistics < ActiveRecord::Migration
  def change
    create_table :account_statistics do |t|
      t.references :account
      t.date    :on_date,                    null: false
      t.decimal :effencive_ratio,            precision: 12, scale: 2
      t.decimal :wn8,                        precision: 12, scale: 2
      t.decimal :wn7,                        precision: 12, scale: 2
      t.integer :battles_count
      t.integer :wins_count
      t.integer :xp
      t.decimal :damage_dealt,               precision: 12, scale: 2
      t.decimal :survived_battles,           precision: 12, scale: 2
      t.decimal :frags,                      precision: 12, scale: 2
      t.decimal :wins_ratio,                 precision: 12, scale: 2

      t.decimal :dropped_capture_points,     precision: 12, scale: 2
      t.decimal :spotted,                    precision: 12, scale: 2
      t.decimal :capture_points,             precision: 12, scale: 2
      #alter table `account_statistics` add column dropped_capture_points decimal(12,2);# Затронуто 2 строки.
      #alter table `account_statistics` add column spotted decimal(12,2);# Затронуто 2 строки.
      #alter table `account_statistics` add column capture_points decimal(12,2);
      t.decimal :damage_avg,                 precision: 12, scale: 2
      t.decimal :xp_avg,                     precision: 12, scale: 2
      t.decimal :xp_max,                     precision: 12, scale: 2
      t.decimal :frage_avg,                  precision: 12, scale: 2
      t.decimal :survived_ratio,             precision: 12, scale: 2
      t.timestamps null: false
    end
  end
end
