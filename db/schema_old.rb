# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "account_stats", primary_key: "stat_id", force: :cascade do |t|
    t.integer "account_id",      limit: 8,                          null: false
    t.date    "on_date",                                            null: false
    t.decimal "effencive_ratio",           precision: 10, scale: 2
    t.decimal "wn8",                       precision: 10, scale: 2
    t.decimal "wn7",                       precision: 10, scale: 2
    t.integer "battles_count",   limit: 4
    t.decimal "wins_ratio",                precision: 10, scale: 2
    t.decimal "damage_avg",                precision: 10, scale: 2
    t.decimal "xp_avg",                    precision: 10, scale: 2
    t.decimal "xp_max",                    precision: 10, scale: 2
    t.decimal "frage_avg",                 precision: 10, scale: 2
    t.decimal "survived_ratio",            precision: 10, scale: 2
  end

  add_index "account_stats", ["account_id", "on_date"], name: "account_id", using: :btree

  create_table "accounts_tanks", primary_key: "account_id", force: :cascade do |t|
    t.string   "user_name", limit: 200, null: false
    t.datetime "date_in",               null: false
  end

  add_foreign_key "account_stats", "accounts_tanks", column: "account_id", primary_key: "account_id", name: "account_stats"
end
