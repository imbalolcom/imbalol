require 'thread'

namespace :tanks_jobs do
  desc "load all info by account"
  #rake tanks_jobs:task_load_users_info
  task task_load_users_info: :environment do

    require File.join(Rails.root, 'app', 'controllers', 'UserApi.rb')
    Account.all().each do |account| 
      UserApi.new( account.user_name).info
      puts "#{account.user_name} #{account.account_id}"
    end
=begin
    threads = []
    idx = 0
    Account.all().each do |account|
      idx += 1
      if idx == 1
        u = UserApi.new( account.user_name)
        u.info
      end
      threads << Thread.new do
        user = UserApi.new( account.user_name)
        user.info
        puts "#{user.user_name} #{user.account_id}"
      end
    end

    threads.each do |thread|
      thread.join
    end
=end

  end

end
